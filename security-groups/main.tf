resource "aws_security_group" "alb"{
    name = "${var.name}-alb-security-group"
    vpc_id = var.vpc_id
    description = "${var.name}-alb-security-group"

    ingress {
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-alb"
    Environment = var.environment
  }
}

resource "aws_security_group" "ecs_tasks" {
  name   = "${var.name}-ecs-task"
  vpc_id = var.vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = var.container_port
    to_port          = var.container_port
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-ecs-task"
    Environment = var.environment
  }
}

resource "aws_security_group" "redis_tasks" {
  name   = "redis-ecs-task"
  vpc_id = var.vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = 6379
    to_port          = 6379
    cidr_blocks      = ["10.0.0.0/16"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "redis-ecs-task"
    Environment = var.environment
  }
}

output "alb_security_group"{
  value = aws_security_group.alb.*.id
}

output "ecs_security_group" {
  value = aws_security_group.ecs_tasks.*.id
}

output "redis_security_group" {
  value = aws_security_group.redis_tasks.*.id
}