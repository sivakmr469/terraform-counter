variable "name" {
  type = string
}
variable "environment" {
  type = string
}

variable "cidr" {
  type = string
}

variable "az" {
  type = list
}

variable "public_subnets" {
  type = list
}

variable "private_subnets" {
  type = list
}