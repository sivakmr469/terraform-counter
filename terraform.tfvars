cidr = "10.0.0.0/16"
name = "counter-project"
environment = "DEV"
az = ["ap-southeast-1a", "ap-southeast-1b"]
private_subnets = ["10.0.0.0/20", "10.0.32.0/20"]
public_subnets      = ["10.0.16.0/20", "10.0.48.0/20"]
container_port = 80