variable "name" {
  default = "echo-project"
  type = string
}

variable "environment" {
  default = "dev"
  type = string
}

variable "cidr" {
  type = string
}

variable "public_subnets" {
  type = list
}

variable "redis_security_group" {
  
}