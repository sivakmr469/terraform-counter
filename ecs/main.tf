resource "aws_ecs_cluster" "main" {
  name = "${var.name}-cluster"
  tags = {
    Name        = "${var.name}-cluster"
    Environment = var.environment
  }
}

resource "aws_ecr_repository" "main" {
  name                 = "${var.name}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

resource "aws_ecr_lifecycle_policy" "main" {
  repository = aws_ecr_repository.main.name

  policy = jsonencode({
    rules = [{
      rulePriority = 1
      description  = "keep last 10 images"
      action       = {
        type = "expire"
      }
      selection     = {
        tagStatus   = "any"
        countType   = "imageCountMoreThan"
        countNumber = 10
      }
    }]
  })
}

resource "aws_ecs_task_definition" "main" {
    family                   = "redis-task"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 1024
    memory                   = 2048
    # task_role_arn            = aws_iam_role.ecs_task_role.arn
    # execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
    container_definitions = jsonencode([{
        name        = "redis-container"
        image       = "docker.io/redis:latest"
        essential   = true
        portMappings = [{
        protocol      = "tcp"
        containerPort = 6379
        hostPort      = 6379
        }]
        # logConfiguration = {
        # logDriver = "awslogs"
        # options = {
        #     awslogs-group         = aws_cloudwatch_log_group.main.name
        #     awslogs-stream-prefix = "ecs"
        #     awslogs-region        = var.region
        # }
        # }
        
    }])
    # runtime_platform = {
    #   operating_system_family = "LINUX"
    #   cpu_architecture        = "X86_64"
    # }
    tags = {
        Name        = "redis-task"
        Environment = var.environment
    }
}

resource "aws_ecs_service" "main" {
  name                               = "redis-service"
  cluster                            = aws_ecs_cluster.main.id
  task_definition                    = aws_ecs_task_definition.main.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  #health_check_grace_period_seconds  = 60
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = var.redis_security_group
    subnets          = var.public_subnets
    assign_public_ip = true
  }

  # we ignore task_definition changes as the revision changes on deploy
  # of a new version of the application
  # desired_count is ignored as it can change due to autoscaling policy
  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}


output "container_image" {
    value = aws_ecr_repository.main.repository_url
}

output "ecs_arn" {
  value = aws_ecs_cluster.main.id
}


